# Hello Docker
An experimental spring-boot hello-world app that is packaged as an image and published to my personal docker hub repo.

# Usage
Login to docker hub and create access token.

Run `mvn spring-boot:build-image -Dspring-boot.build-image.publish=true -Ddocker-username=${USERNAME} -Ddocker-token=${TOKEN}`

Replace `${USERNAME}` and `${TOKEN}` with the docker hub username and the access token created earlier. 

